import numpy as np
import torch
import torch.nn as nn
from torch.distributions.multivariate_normal import MultivariateNormal


class MLP(nn.Module):
    def __init__(
        self, input_dim, hidden, output_dim, depth=1, init_std=None, dropout=None
    ):
        super().__init__()
        self.layers = nn.ModuleList()
        self.sequential(input_dim, hidden, output_dim,
                        depth, init_std, dropout)

    def linear(self, input_dim, output_dim, init_std=None):

        net = nn.Linear(input_dim, output_dim)
        if init_std is not None:
            torch.nn.init.normal_(net.weight, std=init_std)
            torch.nn.init.normal_(net.bias, std=init_std)
        return net

    def sequential(self, input_dim, hidden, output_dim, depth, init_std, dropout):

        self.layers.append(self.linear(input_dim, hidden, init_std))
        for _ in range(depth):
            self.layers.append(nn.LeakyReLU())
            self.layers.append(self.linear(hidden, hidden, init_std))
            if dropout is not None:
                self.layers.append(nn.Dropout(dropout))
        self.layers.append(nn.LeakyReLU())
        self.layers.append(self.linear(hidden, output_dim, init_std))
        return self.layers

    def forward(self, x):

        for layer in self.layers:
            x = layer(x)
        return x


class R_NVP(nn.Module):
    def __init__(
        self, d, k, input_dim, hidden, depth, base_dist, init_std=None, dropout=None
    ):
        super().__init__()
        self.d, self.k = d, k
        self.base_dist = base_dist
        self.sig_net = MLP(
            input_dim,
            hidden,
            d - input_dim,
            depth=depth,
            init_std=init_std,
            dropout=dropout,
        )
        self.mu_net = MLP(
            input_dim,
            hidden,
            d - input_dim,
            depth=depth,
            init_std=init_std,
            dropout=dropout,
        )

    def forward(self, x, flip=False):
        x1, x2 = x[:, : self.k], x[:, self.k:]

        if flip:
            x2, x1 = x1, x2

        # forward
        sig = self.sig_net(x1)
        log_jacob = sig.sum(-1)
        z1, z2 = x1, x2 * torch.exp(sig) + self.mu_net(x1)
        if flip:
            z2, z1 = z1, z2

        z_hat = torch.cat([z1, z2], dim=-1)
        return z_hat, log_jacob

    def inverse(self, z, flip=False):
        z1, z2 = z[:, : self.k], z[:, self.k:]

        if flip:
            z2, z1 = z1, z2

        x1 = z1
        x2 = (z2 - self.mu_net(z1)) * torch.exp(-self.sig_net(z1))
        jacob=-self.sig_net(z1).sum(-1)

        if flip:
            x2, x1 = x1, x2
        return (torch.cat([x1, x2], -1),jacob)


class BatchNorm(nn.Module):
    def __init__(self, d, eps=1e-6, momentum=0.9):
        super().__init__()
        shape = (1, d)
        self.gamma = nn.Parameter(torch.ones(shape))
        self.beta = nn.Parameter(torch.zeros(shape))
        self.moving_mean = torch.zeros(shape)
        self.moving_var = torch.ones(shape)
        self.eps = eps
        self.momentum = momentum

    def batch_norm(self, x, gamma, beta, moving_mean, moving_var):

        if not torch.is_grad_enabled():
            x_hat = (x - moving_mean) / torch.sqrt(moving_var + self.eps)
            var = moving_var

        else:
            mean = x.mean(dim=0)
            var = ((x - mean)**2).mean(dim=0)
            x_hat = (x - mean) / torch.sqrt(var + self.eps)
            with torch.no_grad():
                # Update the mean and variance using moving average
                moving_mean = self.momentum * moving_mean + \
                    (1.0 - self.momentum) * mean
                moving_var = self.momentum * moving_var + \
                    (1.0 - self.momentum) * var

        #y = gamma * x_hat + beta  # Scale and shift

        return x_hat, var, moving_mean.data, moving_var.data

    def forward(self, x):

        # Save the updated `moving_mean` and `moving_var`
        y, current_var, self.moving_mean, self.moving_var = self.batch_norm(
            x, self.gamma, self.beta, self.moving_mean, self.moving_var)

        log_jacob = torch.sum(-0.5*torch.log(current_var+self.eps), dim=-1)
        return y, log_jacob

    def inverse(self, y):
        #x_hat = torch.div(y-self.beta, self.gamma)
        log_jacob = torch.sum(0.5*torch.log(self.moving_var+self.eps), dim=-1)
        return y*torch.sqrt(self.moving_var+self.eps)+self.moving_mean, log_jacob


class stacked_NVP(nn.Module):
    def __init__(self, d, k, hidden, n, base_dist, depth=2, batch_norm=False, init_std=None, dropout=None):
        super().__init__()
        self.bijectors = nn.ModuleList(
            [
                R_NVP(d, k, d - k, hidden, depth, base_dist, init_std, dropout)
                if i % 2
                else R_NVP(d, k, k, hidden, depth, base_dist, init_std, dropout)
                for i in range(n)
            ]
        )
        self.flips = [False if i % 2 else True for i in range(n)]
        self.batch_norm = batch_norm
        self.d = d
        
        if self.batch_norm:
            for i in np.arange(2, n-1, 2):
                self.bijectors.insert(i, BatchNorm(d))
                self.flips.insert(i, None)

    def forward(self, x,return_lj_list=False):
        log_jacobs = []

        for bijector, f in zip(self.bijectors, self.flips):
            if f is not None:
                x, lj = bijector(x, flip=f)
            else:
                x, lj = bijector(x)
            log_jacobs.append(lj)
        if return_lj_list:
            return x, log_jacobs, sum(log_jacobs)
        else:
            return x, sum(log_jacobs)

    def inverse(self, z, return_lj_list=False):
        log_jacobs=[]
        for bijector, f in zip(reversed(self.bijectors), reversed(self.flips)):
            if f is not None:
                z ,lj = bijector.inverse(z, flip=f)
            else:
                z, lj = bijector.inverse(z)
            log_jacobs.append(lj)
        if return_lj_list:
            return z, log_jacobs, sum(log_jacobs)
        else:
            return z, sum(log_jacobs)
        return z, sum(log_jacobs)
