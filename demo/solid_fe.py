from __future__ import division
import sys

sys.path.append("../")
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import MDAnalysis as MDA
from torch.distributions.multivariate_normal import MultivariateNormal
import os
from src.sampling import (
    MonteCarloSampler,
    stdG,
    gaussian_on_lattice,
    lattice,
)
from src.RNVP import stacked_NVP
from src.MCMC_w_flow import MCMC_w_flow

traj = MDA.coordinates.XYZ.XYZReader("../data_for_nf/positions_lj_10K.xyz")
pos = torch.from_numpy(np.array([np.array(traj[i]) for i in range(len(traj))])).flatten(start_dim=1)
pe = np.loadtxt("../data_for_nf/pe_lj_10K.dat")
pe = torch.from_numpy(np.array(pe))

def train(model,sampler,epochs,batch_size,optim,scheduler,save_dir):
    losses=[]
    min_loss=-200
    for k in range(epochs):
        with torch.no_grad():
            samples,energy=sampler.get_samples(batch_size)
        optim.zero_grad()
        z, log_jacob = model(samples)
        log_pz=base.log_prob(z.reshape(batch_size,-1,3))
        print(log_pz)
        print(
                             torch.linalg.norm(
                                 model.inverse(z)[0]- samples
                             ),
                         )
        loss = (-log_pz - log_jacob).mean()
        print(loss)
        if loss < min_loss:
            min_loss=loss
            torch.save({"model":model.state_dict(),"optim":optim.state_dict(),"losses":losses},save_dir)
        losses.append(loss)
        loss.backward()
        optim.step()
        scheduler.step()
    return losses

n=256
kB= 8.6173*10e-5
T=10
beta=1/(kB*T)
lj=lattice(n,pos,pe*n,beta)
base = gaussian_on_lattice(4,4,4)
model_lj = stacked_NVP(3*n, 3*128, hidden=600, n=2, base_dist=base, batch_norm=False)
optim = torch.optim.Adam(model_lj.parameters(), lr=1e-4)
scheduler = torch.optim.lr_scheduler.ExponentialLR(optim, 0.999)
losses=train(
    model_lj,
    lj,
    5000,
    batch_size=20,
    optim=optim,
    scheduler=scheduler,
    save_dir='../saved_models/mixgaussian_lj.pth',
)
