from __future__ import division
import sys
sys.path.append("../")
from src.MCMC_w_flow import MCMC_w_flow
from src.RNVP import stacked_NVP
from src.sampling import (
    MonteCarloSampler,
    stdG,
    gaussian_on_lattice
)
import os
from torch.distributions.multivariate_normal import MultivariateNormal
import MDAnalysis as MDA
import torch.nn as nn
import torch
import numpy as np
import matplotlib.pyplot as plt



def LJ_potential(particle_pos, box_length, epsilon=1., sigma=1.):
    """Calculates Lennard_Jones force between particles
    Arguments:
        particle_pos: A tensor of shape (n_particles, n_dimensions)
        representing the particle positions
        box_length: A tensor of shape (1) representing the box length
        epsilon: A float representing epsilon parameter in LJ
    Returns:
        total_force_on_particle: A tensor of shape (n_particles, n_dimensions)
        representing the total force on a particle
    """
    pair_dist = (particle_pos.unsqueeze(-2) - particle_pos.unsqueeze(-3))
    to_subtract = ((torch.abs(pair_dist) > 0.5 * box_length)
                   * torch.sign(pair_dist) * box_length)
    pair_dist -= to_subtract
    distances = torch.linalg.norm(pair_dist.float(), axis=-1)
    scaled_distances = distances + (distances == 0)
    pair_potential = epsilon * 4 * (torch.pow(sigma/scaled_distances, 12)
                                    - torch.pow(sigma/scaled_distances, 6))
    pair_potential = pair_potential / scaled_distances*distances
    total_potential = torch.sum(pair_potential)/2-torch.max(pair_potential)

    return total_potential


def generate_from_nf(nparticles, nsamples=50):
    base = stdG(3*nparticles)
    nf = torch.load("../saved_models/lj.pth")
    model_lj = stacked_NVP(3*nparticles, 128*3, hidden=600,
                           n=2, base_dist=base, batch_norm=False)
    model_lj.load_state_dict(nf["model"])
    base_samples = base.get_samples(nsamples)
    push_forward, log_jacob = model_lj.inverse(base_samples)
    log_pz = base.log_prob(base_samples)
    log_px = log_pz+log_jacob
    push_back, log_jacob = model_lj.forward(push_forward)
    positions = push_forward.detach().reshape(-1, nparticles, 3)
    return positions


def load_md_data():
    traj = MDA.coordinates.XYZ.XYZReader("../data_for_nf/positions_lj_10K.xyz")
    pos = torch.from_numpy(
        np.array([np.array(traj[i]) for i in range(len(traj))]))
    print(LJ_potential(pos[2], 4, 0.00043, 0.684))
    pe = np.loadtxt("../data_for_nf/pe_lj_10K.dat")
    pe = torch.from_numpy(np.array(pe))
    print(pe[2])


def main():
    nparticles = 256
    kB = 8.6173*10e-5
    T = 10
    beta = 1/(kB*T)
    base = gaussian_on_lattice(1, 1, 2)
    samples=base.get_samples(2)
    print(samples)
    print(base.log_prob(samples))
    #pos=generate_from_nf(nparticles, nsamples=50)
    # print(LJ_potential(pos[3],4,0.00043,0.684))


if __name__ == "__main__":
    main()

'''
n=256
kB= 8.6173*10e-5
T=10
beta=1/(kB*T)
base = stdG(3*n)
nf=torch.load("../saved_models/lj.pth")
model_lj = stacked_NVP(3*n, 128*3, hidden=600, n=2, base_dist=base, batch_norm=False)
model_lj.load_state_dict(nf["model"])
base_samples=base.get_samples(50)
push_forward,log_jacob=model_lj.inverse(base_samples)
log_pz=base.log_prob(base_samples)
log_px=log_pz+log_jacob
push_back,log_jacob=model_lj.forward(push_forward)
positions=push_forward.detach().numpy()
with open('positions.xyz','a') as pos:
    for i in range(50):
        pos.write('256\n')
        pos.write('Atoms. Timestep: %d\n'%i)
        config=np.column_stack((np.ones(256),positions[i].reshape((-1,3))))
        np.savetxt(pos,config, fmt=['%u','%.5f','%.5f','%.5f'])
traj = MDA.coordinates.XYZ.XYZReader("../data_for_nf/positions_lj_30K.xyz")
pos = torch.from_numpy(np.array([np.array(traj[i]) for i in range(len(traj))])).flatten(start_dim=1)
pe = np.loadtxt("../data_for_nf/pe_lj_30K.dat")
pe = torch.from_numpy(np.array(pe))
push_back,lj=model_lj(pos)
log_pz=base.log_prob(push_back)
print("base:",torch.mean(log_pz).detach(),torch.std(log_pz).detach())
print("jacobian", torch.mean(lj).detach(),torch.std(lj).detach())
fe=(-log_pz-lj)/n
print("estimated free energy:",torch.mean(fe).detach(),torch.std(fe).detach())
print("log of normalizing constant:", torch.mean(fe-pe).detach(),torch.std(fe-pe).detach())
'''
